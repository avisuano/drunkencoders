﻿*Class Project Retrospective*

**The Prime Directive of Retrospective**
"Regardless of what we discover, we understand and truly believe that everyone did the best job they could, given what they knew at the time, their skills and abilities, the resources available, and the situation at hand." --Norm Keith

**Safety Check**
During the safety check performed during the meeting, all members presented a 5. The team feels comfortable enough to talk about anything that comes up. We try to encourage each other, and where we are able, offer support to complete the job at hand.

**What we learned from the sprint?**
* This sprint we learned that backlog items need to put in place and broke down in a manner that every member of the team can be assigned a task (or   choose a task) that is detailed to what needs to be completed, what is not to be completed, and what constitutes the task being completed.           Creating these user stories and tasks has been on on-going learning experience.

* We learned that even good intentions fall short.

* The team needs to be better organized, and we must use our time together to our benefit. 

* Work needs to be delegated to all team members so that some people don't feel left out and others don't feel like they carry the burden of the       project

* We need to work more efficiently as a team. Our communication is excellent, but our work has not always shown how well we get along together.

* During this last sprint, we learned that other classes took up a great deal of our time. Not to use it as an excuse, we already mentioned that we have to improve our time management, but we all fell into a CS360 lab that still haunts us to this day. 

**What still isn't going right?**
We are still learning Azure DevOps and trying to get a grasp of how it works. After our last meeting, things became a little clearer and hope to continue to improve our organizational skills, the ability to break work apart, and detail our work or non-work better. Another issue is our understanding of continuous development/deployment. We set up a couple different ways to launch continuous deployment, but we're not sure we actually got the feature to work as intended. The last thing is we feel there are not enough hours in the day to do all the things we want to do. Yet again, we feel that better organization and time management may resolved these issues.

**What can the team do better on the next sprint?**
*Action Plan*
Our team has identified the root causes for our lack of performance which includes:
* Lack of organization within our user stories/tasks

* Time Management

* Poor use of team meetings

As we move forward into our group project, we plan to improve these root causes by:
* Better organizing our user stories/tasks by breaking them down into realistic chunks that can be accomplished within the given time constraints (sprint). By asking questions and working as a team to envision what a task involves and how to know when that task is considered complete, we feel that we will be able to complete our group project one task at a time.

* For time management, all team members need to set aside a set amount of hours each day to complete a few tasks. By not allowing other work to interfere with that time block, we should be able to consistently generate quality work on a daily basis.

* As a team, we need to come together and create a common vision for our project. Without a vision, it will be a difficult task trying to build a site which is meant to be very important for our service men and women. To achieve this goal, we need to make better use of our meeting times by using white boards more often so everyone can have a better understanding of our project and what we are trying to achieve. The use of white boards will help us break down epics, features, user stories, and tasks as to provide more details as to what is needed to complete our project.

**Are there any items that need to be brought up by someone outside the team?**
Not at this time, but if someone would like to bring us donuts and beer, we would be greatly appreciative.

**Other thoughts**
***Things that went well***
While talking about what went well, we came to these conclusions:
* Our communication was very good. We had close to daily contact using Slack, we met during our predetermined meeting times, we utilized the library to meet outside of our meeting times, and we used Hangouts to meet if we weren't on campus. 

* We were able to get most of our modeling/inception requirements completed on time

* Everyone got a chance to learn about each other; our strengths and weaknesses

* The ability to learn how to work together using an Agile Scrum methodology

* Figuring out work-arounds for issues as they came up

***What didn't go well?***
We broke things down into two sections with sub-content:
* Time Management:
    * Allowing other classes to invade on our project time
    * Not discussing the project in more detail at meeting times as a team
* Coding/Design:
    * Some features were not implemented due to lack of knowledge/not enough research
    * Page design could have been improved-too plain
    * Need more detailed models/diagrams
    * Lack of white board utilization to achieve a common vision over the entire project

After the class project was completed, our team realized the importance of working as a team and building a proper backlog. As we move forward, we will be working together as a team to overcome our inadequacies in the class project. We feel that we will be successful in building our group project because of the discoveries we made that will make our team stronger.

