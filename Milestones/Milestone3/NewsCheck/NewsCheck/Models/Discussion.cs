namespace NewsCheck.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Discussion
    {
        [Key]
        public int DiscussionsID { get; set; }

        [Required]
        public string SourceLink { get; set; }

        public int CommentsID { get; set; }

        public virtual Comment Comment { get; set; }
    }
}
