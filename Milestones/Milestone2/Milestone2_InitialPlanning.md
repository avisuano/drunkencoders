## Initial Requirements Elaboration and Elicitation
### Questions

1.  How do we link a discussion on the site to one or more articles/pages?

    We were thinking via URL.

2.  How will users find out that there is a discussion on the site for the article/page they're currently viewing?

    How about a browser plug-in? It could send the URL of the current page to our API to see if a discussion page exists and provide an easy way for them to navigate to our page.

    Or the user can copy the URL and paste it into a search bar on our site.

	Or the user can search keywords from news article titles

3.  Clearly we need accounts and logins. Our own, or do we allow logging in via 3rd party, i.e. "Log in with Google" or ...?

    Our own login account system

4.  Do we allow people to comment anonymously? Read anonymously?

	Only logged accounts can post comments or start discussions. Reading anonymously is fine.

5.  Do we allow people to sign up with a pseudonym or will we demand/enforce real names?

	Pseudonyms can be allowed, fake names and emails are easy to use.

6.  What is it important to know about our users? What data should we collect?

	Comment history and discussion creation history. Some sort of rating system to indicate the validity of comment and discussion history.

7.  If there are news articles on multiple sites that are about the same topic should we have separate discussion pages or just one?

	Multiple stories should be merged into one large discussion page after a certain threshold is passed, using a number system to keep articles separated for easier discussions.

8.  What kind of discussion do we want to create? Linear traditional, chronological, ranked, or ?

	Perhaps a combination of linear traditional with a ranked system that makes well thought out and reasoned comments standout.

9.  Should we allow image/video uploads and host them ourselves?

	Not at this time

10. How are user's expertise going to be verified and rated?

	Some sort of rating system (stars, arrows, thumbs) that will indicate the user is trustworthy?

11. Will users be allowed to find other users?

	Maybe

12. Will users be able to private chat?

	Hard no at the time.

13. How will questions/comments be vetted?

	Perhaps some sore of flag system with a minor justification

14. Bans?

	Partial bans (suspensions), for serious offense bans and IP bans.

15. Character limit for comments and discussions?

	 Perhaps 5 to 10 thousand characters

## List of Needs and Features

1.  A great looking landing page with info to tell the user what our site is all about and how to use it. Include a link to and a page with more info. Needs a page describing our company and our philosophy.
2.  The ability to create a new discussion page about a given article/URL. This discussion page needs to allow users to write comments.
3.  The ability to find a discussion page.
4.  User accounts
5.  A user needs to be able to keep track of things they've commented on and easily go back to those discussion pages. If someone rates or responds to their comment we need to alert them.
6.  Allow users to identify fundamental questions and potential answers about the topic under discussion. Users can then vote on answers.
7. A way to ignore or follow certain users.
8. A system to filter comments/discussions with derogatory or hate speech from being posted
9. A flag system to help the moderators to find comments that slip through the cracks.

## Identify Functional Requirements (User Stories)
E: Epic  
F: Feature  
U: User Story  
T: Task

1.  [U] As a visitor to the site I would like to see a fantastic and modern homepage that tells me how to use the site so I can decide if I want to use this service in the future.
2.  [T] Create starter ASP dot NET MVC 5 Web Application with Individual User Accounts and no unit test project
3.  [T] Switch it over to Bootstrap 4
4.  [T] Create nice homepage: write content, customize navbar
5.  [T] Create SQL Server database on Azure and configure web app to use it. Hide credentials.
6.  [U] Fully enable Individual User Accounts
7.  [T] Copy SQL schema from an existing ASP.NET Identity database and integrate it into our UP script
8.  [T] Configure web app to use our db with Identity tables in it
9.  [T] Create a user table and customize user pages to display additional data
10.  [F] Allow logged in user to create new discussion page
11.  [F] Allow any user to search for and find an existing discussion page
12.  [E] Allow a logged in user to write a comment on an article in an existing discussion page
13.  [E] Rating system to indicate the user's trust, quality of a comment, or if any negative/insensitive language is used, all proper justification.
	    1.  [F] Allow a system for other users to see another user's trustworthiness.
		    1. [U] As a user I want to be able to know if a user can be trusted or not, so I know to avoid or seek out that users comments.
			   1. [T] Put an icon or some sort of indicator next to a user's display name.
	    2.  [F] Allow a logged in user to rate the quality of a comment.
		    1. [U] As a user I want the ability to rate a comment based on it's content, so that others know the comment was valid or one that should probably be ignored.
			    1. [T] Implement up and down arrows that appear on the comment box
	    3. [F] Allow a logged in user to flag a comment that contains. negative/insensitive/hateful/derogatory language or improperly down voted.
		    1. [U] As a user I want to be able to flag a comment that may contain offensive language, a problem with the source, or some other problem with the comment, so that the moderators can remove that comment.
			    1. [T] Put a flag icon on the comment box.
			    2. [T] Implement an alert system to notify the moderators.   
	    4. [F] A drop down menu with predetermined choices to give justification for the rating or flagging of a comment and the trustworthiness of a user.
		    1. [T] Add a pop up box when the positive vote is pushed, with a drop down menu to justify the push.
		    2. [T] Add a pop up box when the negative vote is pushed, with a drop down menu to to justify the push.
		    3. [T] Add a pop up box when the flag is pushed, with a drop down menu to indicate why and alert moderators about a comment.
14.  [U] As a robot I would like to be prevented from creating an account on your website so I don't ask millions of my friends to join your website and add comments about male enhancement drugs.
15. [U] As a user, I want the ability to ignore certain users, so I don't have to deal with their negative comments.
	1. [T] Add a storage space or list for users to have a way to ignore users they have a problem with.
16. [U] As a user, I want the ability to follow certain users, so I can see what they have to say about certain topics.
17. [F] A conformation email that your flagged comment has received and being looked into by the moderation team.
18. [U] As a logged-in user, I would like to create a discussion page where I can post a link to a new story, so I can leave a comment about a inconsistency within the story.
19. [U] As a user, I would like the ability to search for existing discussion pages, so I can read and/or reply to discussions.
20. [U] As a user, I would like the ability to filter through discussions by specific users or topics, so I can retrieve those queries faster than going through every discussion looking for the items I want.
21. [U] As a user, I would like the home page to contain the top ten discussion links, so I can read the comments about those discussions.
22. [U] As a user, I would like an area on the home page to display the hot topics of the day, so I know what people are currently talking about.
23. [U] As a user, I would like a way to change my own password, so I can forget it as much as I want.

## Identify Non-Functional Requirements

1. User accounts and data must be stored indefinitely.
2. Site and data must be backed up regularly and have failover redundancy that will allow the site to remain functional in the event of loss of primary web server or primary database. We can live with 1 minute of complete downtime per event and up to 1 hour of read-only functionality before full capacity is restored.
3. Site should never return debug error pages. Web server must never return 404's. All server errors must be logged. Users should receive a custom error page in that case telling them what to do.
4. Must work in all languages and countries. English will be the default language but users can comment in their own language and we may translate it.
5. Security is of up most important, long passwords with requirements of at least one upper case letter, one lower case letter, one number, and one special character to be used.
6. The site should be simple and intuitive to use, i.e. minimal clutter
7. This site should work on common browsers, such as Firefox, Chrome, Edge (maybe?), Safari, etc..



## Initial Architecture Envisioning
1. ASP.net MVC 5
2. C#
3. SQL Server
4. Azure Web Platform and Services
5. Web Browser
6. API's
7. The internet

## Agile Data Modeling
Scrum with Agile Principles
