﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WhiskeyTangoFoxtrot.Controllers;
using WhiskeyTangoFoxtrot.Repositories.Interfaces;
using System.Linq;
using WhiskeyTangoFoxtrot.Models;
using WhiskeyTangoFoxtrot.DAL;
using System.Collections.Generic;

namespace WTFTests
{
    [TestClass]
    public class AVUnitTest
    {
        [TestMethod]
        public void BoundingBoxHelperMethodReturnsTrue()
        {
            // Define
            decimal a = 1;
            decimal b = 2;

            // Create 
            VAFacilitySearchController vafs = new VAFacilitySearchController();

            // Assert
            var result = vafs.FacilitySearchHelper(a, b);
            Assert.AreEqual(a + (decimal)1.5, result[0]);
            Assert.AreEqual(a + (decimal)0.5, result[1]);
            Assert.AreEqual(b - (decimal)0.5, result[2]);
            Assert.AreEqual(b - (decimal)1.5, result[3]);
        }

        [TestMethod]
        public void BoundingBoxHelperMethodReturnsFalse()
        {
            // Define
            decimal a = 1;
            decimal b = 2;

            // Create 
            VAFacilitySearchController vafs = new VAFacilitySearchController();

            // Assert
            var result = vafs.FacilitySearchHelper(a, b);
            Assert.AreNotEqual(a - 2, result[0]);
            Assert.AreNotEqual(a - 1, result[1]);
            Assert.AreNotEqual(b + 1, result[2]);
            Assert.AreNotEqual(b + 2, result[3]);
        }
    }

    [TestClass]
    public class CityTest
    {
        [TestMethod]
        public void CanGetCity()
        {
            Mock<ICityRepository> mock = new Mock<ICityRepository>();

            mock.Setup(a => a.Cities).Returns(new City[] {
                new City { CityID = 1, City1 = "TestCity, USA", Latitude = 45, Longitude = -123 },
                new City { CityID = 2, City1 = "SomeWhere, USA", Latitude = 47, Longitude = -118 },
                new City { CityID = 3, City1 = "OverThe, USA", Latitude = 31, Longitude = -80 },
                new City { CityID = 4, City1 = "Rainbow, USSR", Latitude = 29, Longitude = -134 }
            });

            VAFacilitySearchController vafs = new VAFacilitySearchController(mock.Object);
            int arrayIndex = mock.Object.Cities.Count();

            Assert.IsTrue(arrayIndex != 0, "Something went wrong.");
            Assert.IsTrue(arrayIndex == 4);

            var id = mock.Object.GetCityIDFromCityName("TestCity, USA");
            Assert.AreEqual(id, 0);

            var lat = mock.Object.GetCityLatitude("TestCity, USA");
            Assert.AreEqual(lat, 0);
        }
    }
}
