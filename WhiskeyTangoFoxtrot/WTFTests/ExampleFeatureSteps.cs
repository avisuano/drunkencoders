﻿using System;
using TechTalk.SpecFlow;

namespace WTFTests
{
    [Binding]
    public class ExampleFeatureSteps
    {
        [Given(@"I have entered ""(.*)"" into the URL")]
        public void GivenIHaveEnteredIntoTheURL(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I press the ""(.*)"" button")]
        public void WhenIPressTheButton(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"the result should be ""(.*)"" in the URL")]
        public void ThenTheResultShouldBeInTheURL(string p0)
        {
            ScenarioContext.Current.Pending();
        }
    }
}
