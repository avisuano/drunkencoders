﻿using WhiskeyTangoFoxtrot.Repositories;
using WhiskeyTangoFoxtrot.Repositories.Interfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WhiskeyTangoFoxtrot.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            this.kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            kernel.Bind<ICityRepository>().To<CityRepository>();
            kernel.Bind<IForumsRepository>().To<ForumRepository>();
            kernel.Bind<IForumPostRepository>().To<ForumPostRepository>();
            kernel.Bind<IWTFUserRepository>().To<WTFUserRepository>();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
    }
}