﻿--------------------
-- ASP.Net Tables --
--------------------

-- ASP.NET Users Table
CREATE TABLE [dbo].[AspNetUsers] (
    [Id]                   NVARCHAR (128) NOT NULL,
    [Email]                NVARCHAR (256) NULL,
    [EmailConfirmed]       BIT            NOT NULL,
    [PasswordHash]         NVARCHAR (MAX) NULL,
    [SecurityStamp]        NVARCHAR (MAX) NULL,
    [PhoneNumber]          NVARCHAR (MAX) NULL,
    [PhoneNumberConfirmed] BIT            NOT NULL,
    [TwoFactorEnabled]     BIT            NOT NULL,
    [LockoutEndDateUtc]    DATETIME       NULL,
    [LockoutEnabled]       BIT            NOT NULL,
    [AccessFailedCount]    INT            NOT NULL,
    [UserName]             NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex]
    ON [dbo].[AspNetUsers]([UserName] ASC);


-- ASP.NET Roles Table
CREATE TABLE [dbo].[AspNetRoles] (
    [Id]   NVARCHAR (128) NOT NULL,
    [Name] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex]
    ON [dbo].[AspNetRoles]([Name] ASC);


-- ASP.NET User Claims Table
CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [UserId]     NVARCHAR (128) NOT NULL,
    [ClaimType]  NVARCHAR (MAX) NULL,
    [ClaimValue] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);
GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserClaims]([UserId] ASC);


-- ASP.NET User Logins Table
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] NVARCHAR (128) NOT NULL,
    [ProviderKey]   NVARCHAR (128) NOT NULL,
    [UserId]        NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC, [UserId] ASC),
    CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);
GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserLogins]([UserId] ASC);


-- ASP.NET User Roles Table
CREATE TABLE [dbo].[AspNetUserRoles] (
    [UserId] NVARCHAR (128) NOT NULL,
    [RoleId] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC),
    CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);
GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserRoles]([UserId] ASC);
GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
    ON [dbo].[AspNetUserRoles]([RoleId] ASC);



-------------------------
-- Tables for our site --
-------------------------


CREATE TABLE [dbo].[WTFUsers] (
	
	[WTFUserID]			INT IDENTITY (1,1)	NOT NULL,
	[Username]			NVARCHAR(64)		NULL,
	[LoginCount]		INT					DEFAULT 1,
	[AspNetIdentityID]	NVARCHAR(128)		NOT NULL,

	CONSTRAINT [PK_dbo.WTFUsers] PRIMARY KEY CLUSTERED ([WTFUserID] ASC)
);

CREATE TABLE [dbo].[UserProfile] (
	[WTFUserID]					INT					NOT NULL,
	[FirstName]					NVARCHAR(64)		NOT NULL,
	[LastName]					NVARCHAR(64)		NOT NULL,
	[UserCity]					NVARCHAR(64)		NULL,
	[Bio]						NVARCHAR (max)		NULL,
	[ProfileIsPrivate]			Bit					NOT NULL,

	CONSTRAINT [PK_dbo.UserProfile.WTFUserID] PRIMARY KEY CLUSTERED	([WTFUserID] ASC),

	CONSTRAINT [FK_dbo.UserProfile.WTFUserID] 
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID]) 
	ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE [dbo].[Forum] (
	[ForumID]		INT				NOT NULL,
	[WTFUserID]		INT				NOT NULL,
	[ForumName]		NVARCHAR(64)	NOT NULL,
	[Message]		NVARCHAR(1024)	NOT NULL,
	[IsActive]		Bit				NULL,
	[IsMemberOnly]	Bit				NULL,

	CONSTRAINT [PK_dbo.Forum.ForumID] PRIMARY KEY CLUSTERED ([ForumID] ASC),

	CONSTRAINT [FK_dbo.Forum.WTFUserID]
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID])
	ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE [dbo].[ForumPost] (
	[ForumPostID]	INT				NOT NULL,
	[WTFUserID]		INT				NOT NULL,
	[ForumID]		INT				NOT NULL,
	[PostMessage]	NVARCHAR(MAX)	NOT NULL,
	[ReplyID]		INT				NULL,
	[Replies]		INT				NULL,

	CONSTRAINT [PK_dbo.Forum.ForumPostID] PRIMARY KEY CLUSTERED ([ForumPostID] ASC),

	CONSTRAINT [FK_dbo.ForumPost.WTFUserID]
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID]),

	CONSTRAINT [FK_dbo.ForumPost.ForumID]
	FOREIGN KEY ([ForumID]) REFERENCES [dbo].[Forum] ([ForumID])
	ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE [dbo].[ChatBox] (
	[ChatBoxID]		INT				NOT NULL,
	[ChatName]		NVARCHAR(64)	NOT NULL,
	[ChatTopic]		NVARCHAR(64)	NOT NULL,
	[Message]		NVARCHAR(256)	NULL,
	[IsMemberOnly]	Bit				NULL,

	CONSTRAINT [PK_dbo.ChatBox.ChatBoxID] PRIMARY KEY CLUSTERED ([ChatBoxID] ASC),
);

CREATE TABLE [dbo].[ChatMessage] (
	[MessageID]			INT			NOT NULL,	
	[WTFUserID]			INT			NOT NULL,
	[ChatBoxID]			INT			NOT NULL,
	[MessageDate]		DateTime	NOT NULL,
	[MessageContent]	NVARCHAR	NOT NULL,
	[ReplyID]			INT			NULL

	CONSTRAINT [PK_dbo.ChatMessage.MessageID] PRIMARY KEY CLUSTERED ([MessageID] ASC),

	CONSTRAINT [FK_dbo.ChatMessage.WTFUserID] 
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID]),

	CONSTRAINT [FK_dbo.ChatMessage.ChatBoxID] 
	FOREIGN KEY ([ChatBoxID]) REFERENCES [dbo].[ChatBox] ([ChatBoxID]),
);

CREATE TABLE [dbo].[Cities](
	[CityID]		INT	IDENTITY(1,1)	NOT NULL,
	[City]			NVARCHAR(32)		NOT NULL,
	[Latitude]		DECIMAL(8,6)		NOT NULL,
	[Longitude]		DECIMAL(9,6)		NOT NULL,

	CONSTRAINT [PK_dbo.CityID] PRIMARY KEY CLUSTERED ([CityID] ASC) 
);

CREATE TABLE [dbo].[SavedCities](
	[WTFUserID]		INT		NOT NULL,
	[CityID]		INT		NOT NULL
	UNIQUE([WTFUserID],[CityID]),

	CONSTRAINT [FK_dbo.WTFUserID]
	FOREIGN KEY ([WTFUserID]) REFERENCES [dbo].[WTFUsers] ([WTFUserID]),

	CONSTRAINT [FK_dbo.CityID]
	FOREIGN KEY ([CityID]) REFERENCES [dbo].[Cities] ([CityID])
);

-----------------------------------------------
-- Seed City information into the City table --
-----------------------------------------------
INSERT INTO Cities VALUES
	-- Washington
	('Tacoma, WA', 47.2529, -122.4443),
	('Seattle, WA', 47.6062, -122.3321),
	('Everett, WA', 47.9790, -122.2021),
	('Mount Vernon, WA', 48.4201, -122.3375),
	('Bellingham, WA', 48.7519, -122.4787),
	-- Oregon
	('Ashland, OR', 42.1946, -122.7095),
	('Medford, OR', 42.3265, -122.8756),
	('Grants Pass, OR', 42.4390, -123.3284),
	('Roseburg, OR', 43.2165, -123.3417),
	('Eugene, OR', 44.0521, -123.0868),
	('Albany, OR', 44.6365, -123.1059),
	('Salem, OR', 44.9429, -123.0351),
	('Portland, OR', 45.5155, -122.6793),
	-- California
	('San Diego, CA', 32.7157, -117.1611),
	('Santa Ana, CA', 33.7455, -117.8677),
	('Los Angeles, CA', 34.0522, -118.2437),
	('Stockton, CA', 37.9577, -121.2908),
	('Sacramento, CA', 38.5816, -121.4944),
	('Redding, CA', 40.5865, -122.3917),
	('Mount Shasta City, CA', 41.3099, -122.3106),
	('Weed, CA', 41.4226, -122.3861),
	('Yreka, CA', 41.7354, -122.6345);