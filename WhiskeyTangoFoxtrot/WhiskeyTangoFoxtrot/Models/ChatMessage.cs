namespace WhiskeyTangoFoxtrot.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChatMessage")]
    public partial class ChatMessage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MessageID { get; set; }

        public int WTFUserID { get; set; }

        public int ChatBoxID { get; set; }

        public DateTime MessageDate { get; set; }

        [Required]
        [StringLength(1)]
        public string MessageContent { get; set; }

        public int? ReplyID { get; set; }

        public virtual ChatBox ChatBox { get; set; }

        public virtual WTFUser WTFUser { get; set; }
    }
}
