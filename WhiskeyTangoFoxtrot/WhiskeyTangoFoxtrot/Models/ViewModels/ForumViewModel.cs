﻿using WhiskeyTangoFoxtrot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WhiskeyTangoFoxtrot.Models.Viewmodels
{
    public class ForumViewModel
    {
        public IEnumerable<WTFUser> WTFUsers { get; set; }
        public IEnumerable<Forum> Forums { get; set; }
        public IEnumerable<ForumPost> ForumPost { get; set; }
    }
}
