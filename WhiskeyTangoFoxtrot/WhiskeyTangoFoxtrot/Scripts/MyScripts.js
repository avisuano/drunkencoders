﻿$(document).ready(function () {
    
 });

    var chat = $.connection.chatHub;

    chat.client.message = function (msg) {
        $('#discussion').append('<li>' + msg + '</li>')                 /*Add the message to the page*/
    }

    $.connection.hub.start(function () {                                /*Start the connection*/
        $('#sendmessage').click(function () {                           /*Post message when button is pressed*/
            chat.server.send($('#message').val());                      /*Call the send message on the hub*/
            $('#message').val('').focus();                              /*Clear text box and set the focus for the next comment*/
        })
    })

 
//$(function () {
  //  var chat = $.connection.chatHub;                                         /*Reference to auto-generated proxy for the hub*/
  //  chat.client.broadcastMessage = function (name, message) {                /*Function that the hub calls back to display message*/
  //      var encodedName = $('<div />').text(name).html();                    /*Set name*/
  //      var encodedMsg = $('<div />').text(message).html();                  /*Set message*/
  //      $('#discussion').append('<div style="text-align: left; color: green;"><li><strong>' + encodedName                 /*Add the message to the page*/
  //          + '</strong>:&nbsp;&nbsp' + encodedMsg + '</li></p>'); 
  //  };

  //  $('#displayname').val(make_anon());                                      /*Get the user name*/
  //  $('#message').focus();                                                   /*Set initial focus to message input box*/
  //  $.connection.hub.logging = true;
  //  $.connection.hub.start().done(function () {                              /*Start the connection*/
  //      $('#sendmessage').click(function () {                                /*Post message when button is pressed*/
  //          chat.server.send($('#displayname').val(), $('#message').val());  /*Call the send message on the hub*/
  //          $('#message').val('').focus();                                   /*Clear text box and set the focus for the next comment*/
  //      });
  //  });
//});

// Function to generate an anonymous name that begins with 'anon' 
// and has an 8-digit random number following
function make_anon() {
    var anonName = "anon" + make_random_number();                            /*Variable to hold the anonymous user's name*/
    $('#welcome_spot').html('Welcome' + ' ' + anonName);                     /*Send the anonymous name to the welcome div*/
    return anonName;                                                         /*Return the anonymous name*/
}

// Function that generates 8-digit random whole numbers
function make_random_number() {
    var number = Math.round(Math.random() * 79999999) + 10000000;            /*Variable to hold the rounded random number*/
    return number;                                                           /*Return the random number*/
}

function whichName() {
    
}

var wasPressed = false;

function getBtnEventLogin() {
    alert("button pressed");
    wasPressed = true;
    return wasPressed;
}

function getBtnEventAnon() {
    wasPressed = true;
    return wasPressed;
}

function PlaySound() {
    var sound = document.getElementById('audio');
    sound.Play();
}

