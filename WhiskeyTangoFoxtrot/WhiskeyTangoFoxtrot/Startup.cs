﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WhiskeyTangoFoxtrot.Startup))]
namespace WhiskeyTangoFoxtrot
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            // Any connection or hub wire up and configuration goes here
             app.MapSignalR();
        }
    }
}
