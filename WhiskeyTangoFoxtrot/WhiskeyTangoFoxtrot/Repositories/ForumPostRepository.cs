using WhiskeyTangoFoxtrot.DAL;
using WhiskeyTangoFoxtrot.Models;
using WhiskeyTangoFoxtrot.Repositories.Interfaces;
using System.Linq;
using System;
using System.Collections.Generic;

namespace WhiskeyTangoFoxtrot.Repositories
{
    public class ForumPostRepository : Repository<ForumPost>, IForumPostRepository
    {
       public ForumPostRepository(WTFContext context) : base(context)
        {

        }

        public ForumPost GetForumPost(int forumPostID)
        {
            return WTFContext.ForumPosts.Where(a => a.ForumPostID == forumPostID).FirstOrDefault();
        }

        public IEnumerable<ForumPost> GetAllForumPosts()
        {
            return WTFContext.ForumPosts.ToList();
        }

        public WTFContext WTFContext
        {
            get { return Context as WTFContext; }
        }

        public IEnumerable<ForumPost> ForumPosts
        {
            get { return WTFContext.ForumPosts; }
        }
    }
}
