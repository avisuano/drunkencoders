using WhiskeyTangoFoxtrot.DAL;
using WhiskeyTangoFoxtrot.Models;
using WhiskeyTangoFoxtrot.Repositories.Interfaces;
using System.Linq;

namespace WhiskeyTangoFoxtrot.Repositories
{
    public class WTFUserRepository : Repository<WTFUser>, IWTFUserRepository
    {
        public WTFUserRepository(WTFContext context) : base(context)
        {

        }

        public WTFUser GetCurrentUser(int id)
        {
            return WTFContext.WTFUsers.Where(a => a.WTFUserID == id).FirstOrDefault();
        }

        public WTFContext WTFContext
        {
            get { return Context as WTFContext; }
        }

    }
}
