﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhiskeyTangoFoxtrot.Repositories.Interfaces;

namespace WhiskeyTangoFoxtrot.Repositories.Persistance
{
    interface IUnitOfWork : IDisposable
    {
        ICityRepository Cities { get; }
        IForumsRepository Forums { get; }
        IForumPostRepository Posts { get; }
        IWTFUserRepository WTFUser { get; }
        int Complete();
    }
}
