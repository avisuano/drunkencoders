﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WhiskeyTangoFoxtrot.DAL;
using WhiskeyTangoFoxtrot.Repositories.Interfaces;

namespace WhiskeyTangoFoxtrot.Repositories.Persistance
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WTFContext _context;

        public UnitOfWork(WTFContext context)
        {
            _context = context;

            Cities = new CityRepository(_context);
            Forums = new ForumRepository(_context);
            Posts = new ForumPostRepository(_context);
            WTFUser = new WTFUserRepository(_context);
        }

        public ICityRepository Cities { get; private set; }

        public IForumsRepository Forums { get; private set; }

        public IForumPostRepository Posts { get; private set; }

        public IWTFUserRepository WTFUser { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}