using System.Collections.Generic;
using WhiskeyTangoFoxtrot.Models;
namespace WhiskeyTangoFoxtrot.Repositories.Interfaces
{

    public interface IForumPostRepository : IRepository<ForumPost>
    {
        ForumPost GetForumPost(int forumPostID);

        IEnumerable<ForumPost> ForumPosts { get; }
     }
}
