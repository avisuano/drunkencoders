﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WhiskeyTangoFoxtrot.Controllers
{
    public class FAQController : Controller
    {
        // GET: FAQ
        public ActionResult FacilityFinder()
        {
            return View();
        }

        public ActionResult Cleanliness()
        {
            return View();
        }

        public ActionResult Inqueries()
        {
            return View();
        }

        public ActionResult Discussion()
        {
            return View();
        }

        public ActionResult LiveChat()
        {
            return View();
        }

        public ActionResult Future()
        {
            return View();
        }
    }
}